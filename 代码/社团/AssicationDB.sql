USE [master]
GO
/****** Object:  Database [AssicationDB]    Script Date: 2020/7/1 17:52:24 ******/
CREATE DATABASE [AssicationDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'AssicationDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\AssicationDB.ndf' , SIZE = 4160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'AssicationDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\AssicationDB_log.ldf' , SIZE = 1280KB , MAXSIZE = 2048GB , FILEGROWTH = 10240KB )
GO
ALTER DATABASE [AssicationDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AssicationDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AssicationDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [AssicationDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [AssicationDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [AssicationDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [AssicationDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [AssicationDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [AssicationDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [AssicationDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [AssicationDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [AssicationDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [AssicationDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [AssicationDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [AssicationDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [AssicationDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [AssicationDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [AssicationDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [AssicationDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [AssicationDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [AssicationDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [AssicationDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [AssicationDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [AssicationDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [AssicationDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [AssicationDB] SET RECOVERY FULL 
GO
ALTER DATABASE [AssicationDB] SET  MULTI_USER 
GO
ALTER DATABASE [AssicationDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [AssicationDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [AssicationDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [AssicationDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [AssicationDB]
GO
/****** Object:  Table [dbo].[Activity]    Script Date: 2020/7/1 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Activity](
	[id] [int] NOT NULL,
	[name] [varchar](20) NULL,
	[image] [varchar](255) NULL,
	[catalogid] [int] NULL,
	[content] [varchar](255) NULL,
	[zipid] [int] NULL,
	[authorid] [int] NULL,
	[posttime] [datetime] NULL,
	[status] [int] NULL,
	[departmentname] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Assocition]    Script Date: 2020/7/1 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Assocition](
	[Aid] [int] NOT NULL,
	[name] [varchar](20) NULL,
	[decription] [varchar](255) NULL,
	[createtime] [datetime] NULL,
	[AMid] [int] NULL,
 CONSTRAINT [PK__Associti__3213E83FA94EB2E6] PRIMARY KEY CLUSTERED 
(
	[Aid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Catalog]    Script Date: 2020/7/1 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Catalog](
	[id] [int] NOT NULL,
	[name] [varchar](20) NULL,
	[description] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 2020/7/1 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comment](
	[id] [int] NOT NULL,
	[content] [varchar](255) NULL,
	[activityid] [int] NULL,
	[memberid] [int] NULL,
	[lastcommentid] [int] NULL,
	[createtime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Department]    Script Date: 2020/7/1 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Department](
	[did] [int] NOT NULL,
	[name] [varchar](20) NULL,
	[description] [varchar](255) NULL,
	[adid] [int] NULL,
	[Aid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[did] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Member]    Script Date: 2020/7/1 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Member](
	[mid] [int] NOT NULL,
	[name] [varchar](20) NULL,
	[pwd] [varchar](20) NULL,
	[class] [varchar](50) NULL,
	[sex] [varchar](2) NULL,
	[description] [varchar](255) NULL,
	[did] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[mid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Record]    Script Date: 2020/7/1 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Record](
	[id] [int] NOT NULL,
	[memberid] [int] NULL,
	[power] [varchar](255) NULL,
	[start] [date] NULL,
	[end] [date] NULL,
	[description] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Zip]    Script Date: 2020/7/1 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Zip](
	[id] [int] NOT NULL,
	[name] [varchar](20) NULL,
	[zip] [varchar](255) NULL,
	[authorid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Activity]  WITH CHECK ADD  CONSTRAINT [fk_author_id] FOREIGN KEY([authorid])
REFERENCES [dbo].[Member] ([mid])
GO
ALTER TABLE [dbo].[Activity] CHECK CONSTRAINT [fk_author_id]
GO
ALTER TABLE [dbo].[Activity]  WITH CHECK ADD  CONSTRAINT [fk_catalog_id] FOREIGN KEY([catalogid])
REFERENCES [dbo].[Catalog] ([id])
GO
ALTER TABLE [dbo].[Activity] CHECK CONSTRAINT [fk_catalog_id]
GO
ALTER TABLE [dbo].[Activity]  WITH CHECK ADD  CONSTRAINT [fk_zip_id] FOREIGN KEY([zipid])
REFERENCES [dbo].[Zip] ([id])
GO
ALTER TABLE [dbo].[Activity] CHECK CONSTRAINT [fk_zip_id]
GO
ALTER TABLE [dbo].[Assocition]  WITH CHECK ADD  CONSTRAINT [fk_adminid_id] FOREIGN KEY([AMid])
REFERENCES [dbo].[Member] ([mid])
GO
ALTER TABLE [dbo].[Assocition] CHECK CONSTRAINT [fk_adminid_id]
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [fk_actityid] FOREIGN KEY([activityid])
REFERENCES [dbo].[Activity] ([id])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [fk_actityid]
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [fk_lastcommnetid] FOREIGN KEY([id])
REFERENCES [dbo].[Comment] ([id])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [fk_lastcommnetid]
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [fk_memberid] FOREIGN KEY([memberid])
REFERENCES [dbo].[Member] ([mid])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [fk_memberid]
GO
ALTER TABLE [dbo].[Department]  WITH CHECK ADD  CONSTRAINT [fk_ad_id] FOREIGN KEY([Aid])
REFERENCES [dbo].[Assocition] ([Aid])
GO
ALTER TABLE [dbo].[Department] CHECK CONSTRAINT [fk_ad_id]
GO
ALTER TABLE [dbo].[Department]  WITH CHECK ADD  CONSTRAINT [fk_aid_id] FOREIGN KEY([adid])
REFERENCES [dbo].[Member] ([mid])
GO
ALTER TABLE [dbo].[Department] CHECK CONSTRAINT [fk_aid_id]
GO
ALTER TABLE [dbo].[Member]  WITH CHECK ADD  CONSTRAINT [fk_departmentid] FOREIGN KEY([did])
REFERENCES [dbo].[Department] ([did])
GO
ALTER TABLE [dbo].[Member] CHECK CONSTRAINT [fk_departmentid]
GO
ALTER TABLE [dbo].[Record]  WITH CHECK ADD  CONSTRAINT [fk_member_id] FOREIGN KEY([memberid])
REFERENCES [dbo].[Member] ([mid])
GO
ALTER TABLE [dbo].[Record] CHECK CONSTRAINT [fk_member_id]
GO
USE [master]
GO
ALTER DATABASE [AssicationDB] SET  READ_WRITE 
GO
